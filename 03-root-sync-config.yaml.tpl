# root-sync.yaml
apiVersion: configsync.gke.io/v1alpha1
kind: RootSync
metadata:
  name: root-sync
  namespace: config-management-system
spec:
  git:
    repo: %%REPO%%
    branch: "master"
    dir: "/cluster-cfg"
    auth: gcenode
  sourceFormat: hierarchy