#!/bin/bash

ACTION=$1
if [ -z "${ACTION}" ]; then
    echo "an 'ACTION' is required (deploy || delete || check)"
    exit 1
fi

if [ -z "${GITLAB_TOKEN_REPO_USER}" ]; then
    echo "Environment variable 'GITLAB_TOKEN_REPO_USER' is required, please set"
    exit 1
fi

if [ -z "${GITLAB_TOKEN_REPO_SYNC}" ]; then
    echo "Environment variable 'GITLAB_TOKEN_REPO_SYNC' is required, please set"
    echo -en "\nRefer to https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#creating-a-personal-access-token to create a token\n\nToken should only be read-only repository\n"
    exit 1
fi

# Script local - 3-digit suffix
export ITERATION_SUFFIX="${ITERATION_SUFFIX-101}"

# optional debug that leaves most state and outputs more logs
export DEBUG="true"
export FANCY_OK="\e[30;48;5;82m OK \e[0m "
export FANCY_FAIL="\e[30;48;5;1m FAIL \e[0m "
export FANCY_NEUTRAL="\e[30;48;5;248m FAIL \e[0m "

# Folder anchor point for the groups of scripts
export WORKDIR=$(cd .. && pwd)

if [ ! -f "${WORKDIR}/vars-${ITERATION_SUFFIX}.sh" ]; then
  echo "Creating a file with all of the variables for this workshop"
cat <<EOT >> ${WORKDIR}/vars-${ITERATION_SUFFIX}.sh
export WORKDIR=$(cd .. && pwd)
export SUFFIX=${ITERATION_SUFFIX}
export PROJECT_ID=$(gcloud config list --format 'value(core.project)')
export PROJECT_NUMBER=$(gcloud projects describe ${PROJECT_ID} --format 'value(projectNumber)')
export REGION=$(gcloud config get-value compute/region 2> /dev/null)
export ZONE=$(gcloud config get-value compute/zone 2> /dev/null)
export ACCOUNT_EMAIL=$(gcloud auth list --filter=status:ACTIVE --format="value(account)")
export ROOT_REPOSITORY_NAME="root-repo-${ITERATION_SUFFIX}"
export USE_ANTHOS="false"
export CNRM_DOWNLOAD=${WORKDIR}/config-sync-setup/cnrm-download
export GITLAB_TOKEN_REPO_SYNC="${GITLAB_TOKEN_REPO_SYNC}"
export GITLAB_TOKEN_REPO_USER="${GITLAB_TOKEN_REPO_USER}"
EOT

fi

source ${WORKDIR}/vars.sh

if [ "${DEBUG}" == "true" ]; then
  echo -en "Environment Variables\n=====================\n"
  cat ${WORKDIR}/vars.sh
fi

# Setup projct environment
function setup_gcp() {
  echo "Enabling required API services"
  gcloud services enable \
      container.googleapis.com \
      containerregistry.googleapis.com \
      cloudresourcemanager.googleapis.com \
      sqladmin.googleapis.com \
      sourcerepo.googleapis.com
}

# Create cluster
function create_cluster() {
  local CLUSTER_EXISTS=$(gcloud container clusters describe "cluster-${SUFFIX}" --format="value(name)" 2> /dev/null)
  if [ -z "${CLUSTER_EXISTS}" ]; then
    # Cluster does not exist
    echo "Creating a new GKE Cluster...(may take 3-10 minutes)"
    gcloud container clusters create "cluster-$SUFFIX" \
        --release-channel rapid \
        --preemptible --enable-ip-alias \
        --workload-pool=${PROJECT_ID}.svc.id.goog \
        --enable-autoscaling --min-nodes 1 --max-nodes 4 --num-nodes 2 \
        --machine-type=n1-standard-4 \
        --labels mesh_id="proj-${PROJECT_NUMBER}" \
        --scopes=cloud-platform \
        --quiet \
        --zone ${ZONE}
    echo -e "${FANCY_OK} Cluster created, use this command to create your 'kubectl' context configuration:"
  else
    echo -e "${FANCY_NEUTRAL} Cluster already exists, moving on"
  fi

  echo "Re-connect to cluster:  gcloud container clusters get-credentials cluster-${SUFFIX} --zone ${ZONE} --project ${PROJECT_ID}"
  gcloud container clusters get-credentials cluster-${SUFFIX} --zone ${ZONE} --project ${PROJECT_ID}
}

# Setup GCP for config connector
function setup_config_connector {
  local CURR_SA_NAME=$(gcloud iam service-accounts describe cnrm-system@${PROJECT_ID}.iam.gserviceaccount.com --format="value(name)" 2> /dev/null)
  if [ -z "$CURR_SA_NAME" ]; then
    # Create a service account that can create resources for this project
    gcloud iam service-accounts create cnrm-system
  else
    gcloud iam service-accounts enable "cnrm-system@${PROJECT_ID}.iam.gserviceaccount.com"
  fi

  # Give permissions to the new service account
  gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:cnrm-system@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/owner"
}

# Install Config Connector into the cluster
function install_config_connector {

  # Using "Namespaced" version
  mkdir -p ${CNRM_DOWNLOAD}

  gsutil cp gs://configconnector-operator/latest/release-bundle.tar.gz ${CNRM_DOWNLOAD}/release-bundle.tar.gz
  tar zxvf ${CNRM_DOWNLOAD}/release-bundle.tar.gz -C ${CNRM_DOWNLOAD}

  kubectl apply -f ${CNRM_DOWNLOAD}/operator-system/configconnector-operator.yaml
  # Wait for all pods to become ready
  kubectl wait -n configconnector-operator-system --for=condition=Ready pod --all

  # install config connector controller
  kubectl apply -f ${WORKDIR}/config-sync-setup/04-config-connector.yaml

  if [ $? -gt 0 ]; then
    echo -e "${FANCY_FAIL} Problems installing cnrm / Config Connector"
    exit 1
  fi

}

function setup_acm() {
  ## Setup user for Cloud Source Repositories
  gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member serviceAccount:${PROJECT_NUMBER}-compute@developer.gserviceaccount.com \
    --role roles/source.reader

  # Install Operator
  if [ "${USE_ANTHOS}" == "true" ]; then
    kubectl apply -f ${WORKDIR}/config-sync-setup/01-config-management-operator-anthos.yaml
  else
    kubectl apply -f ${WORKDIR}/config-sync-setup/01-config-sync-operator.yaml
  fi
  # Wait for operator deployment to complete
  kubectl wait --for=condition=available --timeout=600s deployment config-management-operator -n kube-system
  # Configure Operator

  if [ "${USE_ANTHOS}" == "true" ]; then
    kubectl apply -f ${WORKDIR}/config-sync-setup/02-config-sync-config-anthos.yaml
  else
    kubectl apply -f ${WORKDIR}/config-sync-setup/02-config-sync-config-config-sync.yaml
  fi

  # Wait for ConfigManagement & RootSync CRDs
  kubectl wait --for=condition=established --timeout=60s crd configmanagements.configmanagement.gke.io
  sleep 30 # apparently it's very shakey on how soon RootSync is ready
  kubectl wait --for=condition=established --timeout=60s crd rootsyncs.configsync.gke.io
}

# Setup "RootSync" object within k8s using newly created root-repository
function configure_root_configmanagement() {
  local CONFIG_REPO="https://source.developers.google.com/p/${PROJECT_ID}/r/${ROOT_REPOSITORY_NAME}"
  # Replace RootSync's repository variable with newly created root-repo and apply
  sed "s|%%REPO%%|${CONFIG_REPO}|g" ${WORKDIR}/config-sync-setup/03-root-sync-config.yaml.tpl | kubectl apply -f -
  echo "Waitin for RootSync to become available" && sleep 30 # TODO: avoid a static wait ...waiting on root-sync to be available
  # Enable Workload Identity
  gcloud iam service-accounts add-iam-policy-binding \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[config-management-system/root-reconciler]" \
    ${PROJECT_NUMBER}-compute@developer.gserviceaccount.com
  # Link KSA to GSA
  kubectl annotate serviceaccount -n config-management-system root-reconciler \
    iam.gke.io/gcp-service-account=${PROJECT_NUMBER}-compute@developer.gserviceaccount.com

  kubectl wait --for=condition=available --timeout=600s deployment root-reconciler  -n config-management-system
  if [ $? -gt 0 ]; then
    echo -e "${FANCY_FAIL} Problems setting up RootSync configuration"
    exit 1
  fi
}

# Setup root configuration repository
function create_root_repository() {

  echo "Creating Root Repository"

  local ROOT_REPO_DIR="${WORKDIR}/${ROOT_REPOSITORY_NAME}"

  # Create Google Source Repo
  gcloud source repos create ${ROOT_REPOSITORY_NAME}

  # Clone newly created repo
  gcloud source repos clone ${ROOT_REPOSITORY_NAME} ${ROOT_REPO_DIR}

  # Copy template repo into folder
  cp -r setup-root-repo/cluster-cfg ${ROOT_REPO_DIR}/

  # Commit and push changes
  cd ${ROOT_REPO_DIR}
  # Configure local repo's user/pass
  git config --local user.email "$ACCOUNT_EMAIL"
  git config --local user.name "GCP Demo"

  # Commit changes
  git add . && git commit -m '[setup agent] Initial install' && git push
  if [ $? -gt 0 ]; then
    echo -e "${FANCY_FAIL} Failed to push initial Root Repository"
    exit 1
  fi
}

function remove_cluster() {
  gcloud container clusters delete "cluster-${SUFFIX}" -q
}

function remove_service_accounts() {
  # Remove IAM binding
  gcloud projects remove-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:cnrm-system@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/owner"
  # Disable service account
  gcloud iam service-accounts disable cnrm-system@${PROJECT_ID}.iam.gserviceaccount.com -q
}

function remove_downloaded() {
  rm -rf ${CNRM_DOWNLOAD}
}

function remove_root_repo() {
  # Remove the GSR repo
  gcloud source repos delete ${ROOT_REPOSITORY_NAME} -q
  # Remove local repo
  rm -rf ${WORKDIR}/${ROOT_REPOSITORY_NAME}
}

function remove_variables() {
  rm -rf ${WORKDIR}/vars.sh
}

####################################################################
####################################################################
####################################################################

if [ "${ACTION}" ==  "delete" ]; then
  remove_root_repo
  remove_service_accounts
  remove_cluster
  remove_downloaded
  remove_root_repo
  remove_variables
  exit 0
fi

if [ "${ACTION}" ==  "deploy" ]; then
  ###          ###
  ### Sequence ###
  ###          ###
  # TODO Reenable this and/or put guards around for idempotency
  # 1. Setup GCP
  setup_gcp

  # 1.1 setup cnrm-system (config-connector)
  setup_config_connector

  # 2. Create a cluster
  create_cluster

  # 3. Create Root Repo to contain operator's clusters-wide configuration
  create_root_repository

  # 4. Install & Setup ACM
  setup_acm

  # 4.1 Install cnrm-system
  install_config_connector

  # 5. Setup ConfigManagement operator
  configure_root_configmanagement

  # 6. Create Namespaces for each application (by running script to deploy all namespaces)
  ${WORKDIR}/config-sync-setup/setup-root-repo/bank-of-anthos-landing-zone.sh deploy

  # Two rather lengthy deployments, when these are both ready, the whole system should be ready
  kubectl wait --for=condition=ready pod --timeout=60s -l statefulset.kubernetes.io/pod-name=accounts-db-0 -n accounts-db
  # frontend service is required for obtaining the IP of the LoadBalancer
  kubectl wait --for=condition=available --timeout=600s deployment frontend -n frontend

  # 7. Verify finshed
  # TODO Add a `kubectl wait` for the loadbalanacer instead of sleeping
  echo "Waiting for frontend load balancer to become available"
  sleep 120
  ${WORKDIR}/config-sync-setup/setup-root-repo/bank-of-anthos-landing-zone.sh check

  # 8. obtain frontend IP address from load balancer
  FRONTEND_ENDPOINT=$(kubectl get svc frontend -n frontend -o jsonpath='{.status.loadBalancer.ingress[0].ip}')

  echo "Go to your frontend at: http://${FRONTEND_ENDPOINT}"
  echo "Default user: testuser"
  echo "Default pass: password"
  echo ""

  # TODO: Perhaps keep for dynamically adding more namespaces later
  if [ "${DEBUG}" == "false" ]; then
    remove_downloaded
  fi
fi

if [ "${ACTION}" ==  "check" ]; then
  ${WORKDIR}/config-sync-setup/setup-root-repo/bank-of-anthos-landing-zone.sh check
  echo "Grabbing endpoint...."
  echo "=============================="
  FRONTEND_ENDPOINT=$(kubectl get svc frontend -n frontend -o jsonpath='{.status.loadBalancer.ingress[0].ip}')
  echo "Go to your frontend at: http://${FRONTEND_ENDPOINT}"
  echo "Default user: testuser"
  echo "Default pass: password"
  echo ""
fi