#!/bin/bash

NEW_NAMESPACE=$1
REPOSITORY=$2
PROJECT_ID=$3
REGION=${4:-us-west3}

if [ -z "${NEW_NAMESPACE}" ]; then
    echo "a 'NAMESPACE' parameter is required"
    exit 1
fi

if [ -z "${REPOSITORY}" ]; then
    echo "a 'REPOSITORY' parameter is required"
    exit 1
fi

if [ -z "${PROJECT_ID}" ]; then
    echo "a 'PROJECT_ID' parameter is required"
    exit 1
fi

function create_serviceaccount() {
    local NAMESPACE=$1
    local CURR_SA_NAME=$(gcloud iam service-accounts describe cnrm-system-${NAMESPACE}@${PROJECT_ID}.iam.gserviceaccount.com --format="value(name)" 2> /dev/null)
    if [ -z "$CURR_SA_NAME" ]; then
        # Create a service account that can create resources for this project
        echo "Creating a Google Service Account for the CNRM (config connector) functionality in ${NAMESPACE}"
        gcloud iam service-accounts create cnrm-system-${NAMESPACE} --description="Workload Identity and CNRM GSA" --display-name="${NAMESPACE} GSA"
    else
        echo "Enabing the Google Service Account for the CNRM functionality in ${NAMESPACE}"
        gcloud iam service-accounts enable "cnrm-system-${NAMESPACE}@${PROJECT_ID}.iam.gserviceaccount.com"
    fi

    # TODO Scope this differently, this is too much control
    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:cnrm-system-${NAMESPACE}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/owner"

    # TODO Remove this hack...sqladmin is needed for creation and access of SQL, but admin isn't needed and should be selective
    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:cnrm-system-${NAMESPACE}@${PROJECT_ID}.iam.gserviceaccount.com" \
    --role="roles/cloudsql.admin"

    # Set workload identity GSA to know about new namespace GSA [k8s-namespace/ksa-name]
    gcloud iam service-accounts add-iam-policy-binding \
        cnrm-system-${NAMESPACE}@${PROJECT_ID}.iam.gserviceaccount.com \
        --member="serviceAccount:${PROJECT_ID}.svc.id.goog[${NAMESPACE}/${NAMESPACE}-ksa]" \
        --role="roles/iam.workloadIdentityUser"

    # Set workload identity for CNRM resources in folder
    gcloud iam service-accounts add-iam-policy-binding \
        cnrm-system-${NAMESPACE}@${PROJECT_ID}.iam.gserviceaccount.com \
        --member="serviceAccount:${PROJECT_ID}.svc.id.goog[cnrm-system/cnrm-controller-manager-${NAMESPACE}]" \
        --role="roles/iam.workloadIdentityUser"

    # Verify pods created for namespace
    kubectl wait -n cnrm-system --for=condition=Ready pod \
        -l cnrm.cloud.google.com/component=cnrm-controller-manager,cnrm.cloud.google.com/scoped-namespace=${NAMESPACE}
}

echo "RootSync creating namespace ${NEW_NAMESPACE} from ${REPOSITORY}"

ROOT_NS_FOLDER=${WORKDIR}/${ROOT_REPOSITORY_NAME}/cluster-cfg/namespaces/bank-of-anthos
# Create new folder for new namespace
mkdir -p ${ROOT_NS_FOLDER}/${NEW_NAMESPACE}
# Setup Namespace
sed -e "s|%%NAMESPACE%%|${NEW_NAMESPACE}|g" -e "s|%%PROJECT_ID%%|${PROJECT_ID}|g" ${WORKDIR}/config-sync-setup/setup-root-repo/landing-zone-template/namespace.yaml.tpl > ${ROOT_NS_FOLDER}/${NEW_NAMESPACE}/namespace.yaml
# Setup RBAC
sed -e "s|%%NAMESPACE%%|${NEW_NAMESPACE}|g" ${WORKDIR}/config-sync-setup/setup-root-repo/landing-zone-template/repo-rbac.yaml.tpl > ${ROOT_NS_FOLDER}/${NEW_NAMESPACE}/repo-rbac.yaml
# Setup RepoSync
sed -e "s|%%NAMESPACE%%|${NEW_NAMESPACE}|g" -e "s|%%REPOSITORY%%|${REPOSITORY}|g" ${WORKDIR}/config-sync-setup/setup-root-repo/landing-zone-template/repo-sync.yaml.tpl > ${ROOT_NS_FOLDER}/${NEW_NAMESPACE}/repo-sync.yaml
# Setup Workload Identity KSA
sed -e "s|%%NAMESPACE%%|${NEW_NAMESPACE}|g" -e "s|%%PROJECT_ID%%|${PROJECT_ID}|g" ${WORKDIR}/config-sync-setup/setup-root-repo/landing-zone-template/workload-identity-ksa.yaml.tpl > ${ROOT_NS_FOLDER}/${NEW_NAMESPACE}/workload-identity-ksa.yaml
# Setup Workload Identity KSA
sed -e "s|%%REGION%%|${NEW_NAMESPACE}|g" -e "s|%%PROJECT_ID%%|${PROJECT_ID}|g" ${WORKDIR}/config-sync-setup/setup-root-repo/landing-zone-template/accounts-db-cnrm-config.yaml.tpl > ${ROOT_NS_FOLDER}/${NEW_NAMESPACE}/accounts-db-cnrm-config.yaml
# Install CNRM
sed -e "s|%%NAMESPACE%%|${NEW_NAMESPACE}|g" -e "s|%%PROJECT_ID%%|${PROJECT_ID}|g" ${WORKDIR}/config-sync-setup/setup-root-repo/landing-zone-template/config-connector-context.yaml.tpl > ${ROOT_NS_FOLDER}/${NEW_NAMESPACE}/config-connector-context.yaml
# Create a service account for the new namespace
create_serviceaccount $NEW_NAMESPACE


echo "added namespace.yaml, repo-rbac.yaml & repo-sync.yaml to ${ROOT_NS_FOLDER}/${NEW_NAMESPACE}"
