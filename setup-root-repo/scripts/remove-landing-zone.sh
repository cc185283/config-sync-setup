#!/bin/bash

NEW_NAMESPACE=$1

if [ -z "${NEW_NAMESPACE}" ]; then
    echo "a 'NAMESPACE' parameter is required"
    exit 1
fi

function delete_serviceaccount() {
    local NAMESPACE=$1
    local CURR_SA_NAME=$(gcloud iam service-accounts describe cnrm-system-${NAMESPACE}@${PROJECT_ID}.iam.gserviceaccount.com --format="value(name)" 2> /dev/null)
    if [ -z "$CURR_SA_NAME" ]; then
        # Create a service account that can create resources for this project
        gcloud iam service-accounts disable cnrm-system-${NAMESPACE}
    else
        gcloud iam service-accounts disable "cnrm-system-${NAMESPACE}@${PROJECT_ID}.iam.gserviceaccount.com"
    fi

    # Adding "editor" access to the newly created GSA
    gcloud projects remove-iam-policy-binding ${PROJECT_ID} \
        --member="serviceAccount:cnrm-system-${NAMESPACE}@${PROJECT_ID}.iam.gserviceaccount.com" \
        --role="roles/editor"

    # Remove the permission for Workload Identity to removed namespace
    gcloud iam service-accounts remove-iam-policy-binding \
        cnrm-system-${NAMESPACE}@${PROJECT_ID}.iam.gserviceaccount.com \
        --member="serviceAccount:${PROJECT_ID}.svc.id.goog[cnrm-system/cnrm-controller-manager-${NAMESPACE}]" \
        --role="roles/iam.workloadIdentityUser"
}

ROOT_NS_FOLDER=${WORKDIR}/${ROOT_REPOSITORY_NAME}/cluster-cfg/namespaces/bank-of-anthos

kubectl delete secret jwt-key -n ${NEW_NAMESPACE}

# remove folder from git repo
rm -rf ${ROOT_NS_FOLDER}/${NEW_NAMESPACE} && true

# Delete or deactivate the service account for the namespace
delete_serviceaccount ${NEW_NAMESPACE}

echo "Removed ${NEW_NAMESPACE}"