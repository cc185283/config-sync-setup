# This Secret is used for configuring the Accounts database. Several services use the configuration
# and refer to them via name. This name is derived from the Bank-of-Anthos KRM files and cannot be changed

# TODO: Un-hardcode the instance name (requires SQLInstance parameterization)
# TODO: Add namespace selector for only the accounts-db, contacts and userservice namespaces (label like 'uses=accounts-db-config')

# Kubernetes secret to avoid changes in all config files
apiVersion: v1
kind: Secret
metadata:
  name: accounts-db-config
stringData:
  password: accounts-pwd
  username: accounts-admin
  connectionName: %%REGION%%:accounts-db-instance-002
  projectId: %%PROJECT_ID%%