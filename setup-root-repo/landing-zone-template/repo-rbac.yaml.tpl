# ROOT_REPO/namespaces/NAMESPACE/sync-rolebinding.yaml
 kind: RoleBinding
 apiVersion: rbac.authorization.k8s.io/v1
 metadata:
   name: syncs-repo
   namespace: "%%NAMESPACE%%"
 subjects:
 - kind: ServiceAccount
   name: "ns-reconciler-%%NAMESPACE%%"
   namespace: config-management-system
 roleRef:
   kind: ClusterRole
   name: cluster-admin # Created by abstract namespace
   apiGroup: rbac.authorization.k8s.io