apiVersion: core.cnrm.cloud.google.com/v1beta1
kind: ConfigConnectorContext
metadata:
  # you can only have one ConfigConnectorContext per Namespace
  name: configconnectorcontext.core.cnrm.cloud.google.com
  namespace: %%NAMESPACE%%
spec:
  # The Google Service Account used to authenticate Google Cloud APIs in this Namespace
  # Convention name based
  googleServiceAccount: "cnrm-system-%%NAMESPACE%%@%%PROJECT_ID%%.iam.gserviceaccount.com"