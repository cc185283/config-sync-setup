# Overview

This is a template of the root repository. Configurations contained within this repository are the highest precedence configurations within the entire Environ (all grouped clusters).

>NOTE: Typically, this folder structure would be generated using `nomos init`, but in order to reduce unnecessary complexity, the folder structure has been generated and will produce a functional git root repository.

# Template

This folder structure will be created into a standalone git repository and will be linked using a `RootSync` kubernetes object into the cluster.

# Folder structure

/cluster - All resources will be created at the Cluster level (ie, not within a namespace and cannot belong to a namespace)
/clusterregistry - Definitions of clusters and selectors allowing KRM resources to be deployed to specific clusters
/system - Versioning capabilities for the ConfigManagement system (do not touch)
/namespaces - Contains all of the namespaces and configurations for each namespace

## Namespaces

